// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

use super::{ReprAbi, ReprWritable, Result};

pub trait ReprWriter {
	fn abi(&self) -> &ReprAbi;
	fn write_bool(&mut self, v: bool) -> Result<()>;
	fn write_i8(&mut self, v: i8) -> Result<()>;
	fn write_u8(&mut self, v: u8) -> Result<()>;
	fn write_i16(&mut self, v: i16) -> Result<()>;
	fn write_u16(&mut self, v: u16) -> Result<()>;
	fn write_i32(&mut self, v: i32) -> Result<()>;
	fn write_u32(&mut self, v: u32) -> Result<()>;
	fn write_i64(&mut self, v: i64) -> Result<()>;
	fn write_u64(&mut self, v: u64) -> Result<()>;
	fn write_f32(&mut self, v: f32) -> Result<()>;
	fn write_f64(&mut self, v: f64) -> Result<()>;
	fn write_isize(&mut self, v: isize) -> Result<()>;
	fn write_usize(&mut self, v: usize) -> Result<()>;
	// fn write_isize(&mut self, v: A::Isize) -> Result<()>;
	// fn write_usize(&mut self, v: A::Usize) -> Result<()>;
	fn write_char(&mut self, v: char) -> Result<()>;
	fn write_owned_ref(&mut self, value: &dyn ReprWritable) -> Result<()>;
	fn write_reference(&mut self, symbol_name: &str) -> Result<()>;

	fn start_array(&mut self) -> Result<&mut dyn ReprSequenceWriter>;
	fn start_struct(&mut self, align: usize) -> Result<&mut dyn ReprSequenceWriter>;
}

pub trait ReprSequenceWriter {
	fn write_element(&mut self, element: &dyn ReprWritable) -> Result<()>;
	fn write_reference(&mut self, element: &dyn ReprWritable) -> Result<()>;
	fn end(&mut self) -> Result<()>;
}
