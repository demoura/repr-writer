// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

use super::{
	Artifact, ArtifactWriterError, ReprAbi, ReprSequenceWriter, ReprWritable, ReprWriter, Result,
};
use byteorder::{ByteOrder, WriteBytesExt};
use core::{convert::TryFrom, iter, marker::PhantomData};
use faerie::{Decl, Link};

pub fn to_global<B, N>(
	artifact: &mut Artifact,
	abi: &ReprAbi,
	name: N,
	value: &dyn ReprWritable,
) -> Result<()>
where
	B: ByteOrder,
	N: AsRef<str>,
{
	let align = u64::try_from(value.align_dyn(abi)).map_err(Into::into)?;

	artifact
		.declare(name.as_ref(), Decl::data().global().with_align(Some(align)))
		.map_err(Into::into)?;

	let mut writer = ArtifactWriter::<'_, B> {
		abi,
		artifact,
		byteorder: PhantomData,
		name: name.as_ref(),
		output: Vec::new(),
		ref_counter: 0,
	};

	value.write(&mut writer).map_err(Into::into)?;

	let artifact = writer.artifact;
	let output = writer.output;
	let name = writer.name;

	artifact.define(name, output).map_err(Into::into)?;

	Ok(())
}

pub struct ArtifactWriter<'w, B: ByteOrder> {
	abi: &'w ReprAbi,
	artifact: &'w mut Artifact,
	byteorder: PhantomData<B>,
	name: &'w str,
	output: Vec<u8>,
	ref_counter: usize,
}

impl<'w, B: ByteOrder> ArtifactWriter<'w, B> {
	pub fn new(artifact: &'w mut Artifact, abi: &'w ReprAbi, name: &'w str) -> Self {
		ArtifactWriter::<'w, B> {
			abi,
			artifact,
			byteorder: PhantomData,
			name,
			output: Vec::new(),
			ref_counter: 0,
		}
	}

	pub fn get_output(self) -> Vec<u8> {
		self.output
	}

	fn pad_to(&mut self, align: usize) {
		let len_mod_align = self.output.len().rem_euclid(align);

		if len_mod_align != 0 {
			let pad = align - len_mod_align;
			self.output.extend(iter::repeat(0).take(pad));
		}
	}
}

impl<'w, B: ByteOrder> ReprWriter for ArtifactWriter<'w, B> {
	fn abi(&self) -> &ReprAbi {
		self.abi
	}

	fn write_bool(&mut self, v: bool) -> Result<()> {
		self.pad_to(self.abi.align_of_bool);
		self.output.write_u8(v as u8).map_err(Into::into)?;
		Ok(())
	}

	fn write_i8(&mut self, v: i8) -> Result<()> {
		self.pad_to(self.abi.align_of_i8);
		self.output.write_i8(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_i16(&mut self, v: i16) -> Result<()> {
		self.pad_to(self.abi.align_of_i16);
		self.output.write_i16::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_i32(&mut self, v: i32) -> Result<()> {
		self.pad_to(self.abi.align_of_i32);
		self.output.write_i32::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_i64(&mut self, v: i64) -> Result<()> {
		self.pad_to(self.abi.align_of_i64);
		self.output.write_i64::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_u8(&mut self, v: u8) -> Result<()> {
		self.pad_to(self.abi.align_of_u8);
		self.output.write_u8(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_u16(&mut self, v: u16) -> Result<()> {
		self.pad_to(self.abi.align_of_u16);
		self.output.write_u16::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_u32(&mut self, v: u32) -> Result<()> {
		self.pad_to(self.abi.align_of_u32);
		self.output.write_u32::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_u64(&mut self, v: u64) -> Result<()> {
		self.pad_to(self.abi.align_of_u64);
		self.output.write_u64::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_f32(&mut self, v: f32) -> Result<()> {
		self.pad_to(self.abi.align_of_f32);
		self.output.write_f32::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_f64(&mut self, v: f64) -> Result<()> {
		self.pad_to(self.abi.align_of_f64);
		self.output.write_f64::<B>(v).map_err(Into::into)?;
		Ok(())
	}

	fn write_char(&mut self, v: char) -> Result<()> {
		self.pad_to(self.abi.align_of_char);
		self.output.write_u32::<B>(v as u32).map_err(Into::into)?;
		Ok(())
	}

	fn write_isize(&mut self, v: isize) -> Result<()> {
		self.pad_to(self.abi.align_of_isize);

		if v >= self.abi.isize_min && v <= self.abi.isize_max {
			self.output
				.write_int128::<B>(
					i128::try_from(v).map_err(Into::into)?,
					self.abi.size_of_isize,
				)
				.map_err(Into::into)
		} else {
			Err(Box::new(ArtifactWriterError::InvalidIsize(v)))
		}
	}

	fn write_usize(&mut self, v: usize) -> Result<()> {
		self.pad_to(self.abi.align_of_usize);

		if v >= self.abi.usize_min && v <= self.abi.usize_max {
			self.output
				.write_uint128::<B>(
					u128::try_from(v).map_err(Into::into)?,
					self.abi.size_of_usize,
				)
				.map_err(Into::into)
		} else {
			Err(Box::new(ArtifactWriterError::InvalidUsize(v)))
		}
	}

	fn write_owned_ref(&mut self, value: &dyn ReprWritable) -> Result<()> {
		self.pad_to(self.abi.align_of_usize);

		let name = format!("{}_REF{}", self.name, self.ref_counter);
		self.ref_counter += 1;
		to_global::<B, _>(&mut self.artifact, self.abi, &name, value).map_err(Into::into)?;

		self.artifact
			.link_with(
				Link {
					from: self.name,
					at: self.output.len() as u64,
					to: &name,
				},
				self.abi.global_ptr_reloc,
			)
			.map_err(Into::into)?;

		// Insert null pointer, linker will replace with correct value.
		self.output
			.resize(self.output.len() + self.abi.size_of_ptr, 0);

		Ok(())
	}

	fn write_reference(&mut self, symbol_name: &str) -> Result<()> {
		self.pad_to(self.abi.align_of_usize);

		self.artifact
			.link_with(
				Link {
					from: self.name,
					at: self.output.len() as u64,
					to: &symbol_name,
				},
				self.abi.global_ptr_reloc,
			)
			.map_err(Into::into)?;

		// Insert null pointer, linker will replace with correct value.
		self.output
			.resize(self.output.len() + self.abi.size_of_ptr, 0);

		Ok(())
	}

	fn start_array(&mut self) -> Result<&mut dyn ReprSequenceWriter> {
		Ok(self)
	}

	fn start_struct(&mut self, align: usize) -> Result<&mut dyn ReprSequenceWriter> {
		self.pad_to(align);
		Ok(self)
	}
}

impl<'w, B: ByteOrder> ReprSequenceWriter for ArtifactWriter<'w, B> {
	fn write_element(&mut self, element: &dyn ReprWritable) -> Result<()> {
		element.write(self)
	}

	fn write_reference(&mut self, element: &dyn ReprWritable) -> Result<()> {
		self.write_owned_ref(element)
	}

	fn end(&mut self) -> Result<()> {
		Ok(())
	}
}
