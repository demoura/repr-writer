// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

use cfg_if::cfg_if;

use super::ArtifactError;

use core::fmt::{self, Debug, Display};

cfg_if! {
	if #[cfg(feature = "std")] {
		use core::num::TryFromIntError;

		type StdError = dyn std::error::Error + Send + Sync + 'static;

		pub trait Error: std::error::Error + Send + Sync + 'static {
			fn as_std_error(&self) -> &StdError;
			fn into_std_error(self: Box<Self>) -> Box<StdError>;
		}

		use std::io::Error as IoError;

		#[derive(Debug)]
		pub enum ArtifactWriterError {
			IoError(IoError),
			ArtifactError(ArtifactError),
			ConversionError(TryFromIntError),
			InvalidUsize(usize),
			InvalidIsize(isize),
			SymbolReferenceUnresolved(Box<StdError>),
		}

		impl From<TryFromIntError> for ArtifactWriterError {
			fn from(error: TryFromIntError) -> ArtifactWriterError {
				ArtifactWriterError::ConversionError(error)
			}
		}
		impl From<IoError> for ArtifactWriterError {
			fn from(error: IoError) -> ArtifactWriterError {
				ArtifactWriterError::IoError(error)
			}
		}

		impl From<ArtifactError> for ArtifactWriterError {
			fn from(error: ArtifactError) -> ArtifactWriterError {
				ArtifactWriterError::ArtifactError(error)
			}
		}

		impl Display for ArtifactWriterError {
			fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
				match self {
					ArtifactWriterError::IoError(error) =>
						write!(f, "Serialization failed due to an I/O error: {}", error),
					ArtifactWriterError::ArtifactError(error) =>
						write!(f, "Serialization failed due to an artifact error: {}", error),
					ArtifactWriterError::ConversionError(error) =>
						write!(f, "Serialization failed due to an conversion error: {}", error),
					ArtifactWriterError::InvalidUsize(v) =>
						write!(f, "Serialization failed due to invalid usize: {}", v),
					ArtifactWriterError::InvalidIsize(v) =>
						write!(f, "Serialization failed due to invalid isize: {}", v),
					ArtifactWriterError::SymbolReferenceUnresolved(error) =>
						write!(f, "Serialization failed due unresolved symbol reference: {}",
							error),
				}
			}
		}

		impl std::error::Error for ArtifactWriterError {
			fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
				match &self {
					ArtifactWriterError::IoError(error) => Some(error),
					ArtifactWriterError::ArtifactError(error) => Some(error),
					ArtifactWriterError::ConversionError(error) => Some(error),
					ArtifactWriterError::InvalidUsize(_) => None,
					ArtifactWriterError::InvalidIsize(_) => None,
					ArtifactWriterError::SymbolReferenceUnresolved(error)
						=> Some(error.as_ref()),
				}
			}
		}

		impl Error for ArtifactWriterError {
			fn as_std_error(&self) -> &StdError {
				self
			}

			fn into_std_error(self: Box<Self>) -> Box<StdError> {
				self
			}
		}

		impl Into<Box<dyn Error>> for TryFromIntError {
			fn into(self) -> Box<dyn Error> {
				Box::new(ArtifactWriterError::from(self))
			}
		}
		impl Into<Box<dyn Error>> for IoError {
			fn into(self) -> Box<dyn Error> {
				Box::new(ArtifactWriterError::from(self))
			}
		}
		impl Into<Box<dyn Error>> for ArtifactError {
			fn into(self) -> Box<dyn Error> {
				Box::new(ArtifactWriterError::from(self))
			}
		}
		impl Into<Box<dyn Error>> for ArtifactWriterError {
			fn into(self) -> Box<dyn Error> {
				Box::new(self)
			}
		}
	} else {
		pub trait Error: Debug + Display {
		}
	}
}
