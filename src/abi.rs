// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

use super::Reloc;

pub struct ReprAbi {
	pub align_of_bool: usize,
	pub align_of_char: usize,

	pub align_of_f32: usize,
	pub align_of_f64: usize,

	pub align_of_i8: usize,
	pub align_of_i16: usize,
	pub align_of_i32: usize,
	pub align_of_i64: usize,
	pub align_of_isize: usize,

	pub align_of_ptr: usize,

	pub align_of_u8: usize,
	pub align_of_u16: usize,
	pub align_of_u32: usize,
	pub align_of_u64: usize,
	pub align_of_usize: usize,

	pub global_ptr_reloc: Reloc,

	pub isize_max: isize,
	pub isize_min: isize,

	pub size_of_isize: usize,
	pub size_of_ptr: usize,
	pub size_of_usize: usize,

	pub usize_max: usize,
	pub usize_min: usize,
}
