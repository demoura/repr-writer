// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

use super::{ReprAbi, ReprWriter, Result};

pub trait ReprWritable {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized;

	fn align_dyn(&self, abi: &ReprAbi) -> usize;

	fn write(&self, writer: &mut dyn ReprWriter) -> Result<()>;
}

macro_rules! primitive_impl {
	($t:ty, $align:ident, $write_func:ident) => {
		impl ReprWritable for $t {
			fn align(abi: &ReprAbi) -> usize
			where
				Self: Sized,
			{
				abi.$align
			}

			fn align_dyn(&self, abi: &ReprAbi) -> usize {
				Self::align(abi)
			}

			fn write(&self, writer: &mut dyn ReprWriter) -> Result<()> {
				writer.$write_func(*self)
			}
		}
	};
}

primitive_impl!(bool, align_of_bool, write_bool);
primitive_impl!(char, align_of_char, write_char);
primitive_impl!(i8, align_of_i8, write_i8);
primitive_impl!(i16, align_of_i16, write_i16);
primitive_impl!(i32, align_of_i32, write_i32);
primitive_impl!(i64, align_of_i64, write_i64);
primitive_impl!(isize, align_of_isize, write_isize);
primitive_impl!(u8, align_of_u8, write_u8);
primitive_impl!(u16, align_of_u16, write_u16);
primitive_impl!(u32, align_of_u32, write_u32);
primitive_impl!(u64, align_of_u64, write_u64);
primitive_impl!(usize, align_of_usize, write_usize);
