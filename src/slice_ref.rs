// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

#[cfg(feature = "std")]
use super::{ReprAbi, ReprWritable, ReprWriter, Result};
use core::slice;

#[repr(C)]
#[derive(Debug)]
pub struct SliceRef<'a, T> {
	data: &'a T,
	len: usize,
}

// Manual implementation of Clone, because T doesn't have
// to also be Clone, as we only clone the reference.
impl<'a, T> Clone for SliceRef<'a, T> {
	fn clone(&self) -> Self {
		Self {
			data: self.data,
			len: self.len,
		}
	}
}

// Manual implementation of Copy, because T doesn't have
// to also be Copy, as we only copy the reference.
impl<'a, T: Clone> Copy for SliceRef<'a, T> {}

impl<'a, T> From<&'a [T]> for SliceRef<'a, T> {
	fn from(slice: &'a [T]) -> SliceRef<'a, T> {
		Self {
			data: &slice[0],
			len: slice.len(),
		}
	}
}

impl<'a, T> core::ops::Deref for SliceRef<'a, T> {
	type Target = [T];

	fn deref(&self) -> &Self::Target {
		unsafe { slice::from_raw_parts::<'a>(self.data as *const T, self.len) }
	}
}

#[cfg(feature = "std")]
struct WriteHelper<'a, T> {
	slice: &'a [T],
}

#[cfg(feature = "std")]
impl<'a, T: ReprWritable> ReprWritable for WriteHelper<'a, T> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		T::align(abi)
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> Result<()> {
		let seq = writer.start_array()?;
		for element in self.slice {
			seq.write_element(element)?;
		}

		seq.end()
	}
}

#[cfg(feature = "std")]
impl<'a, T: ReprWritable> ReprWritable for SliceRef<'a, T> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		usize::max(abi.align_of_ptr, abi.align_of_usize)
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;
		seq.write_reference(&WriteHelper { slice: &**self })?;
		seq.write_element(&self.len)?;
		seq.end()
	}
}
