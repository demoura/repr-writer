// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use fixed::{FixedI16, FixedI32, FixedI64, FixedI8, FixedU16, FixedU32, FixedU64, FixedU8};

use crate::ReprWritable;

impl<Frac> ReprWritable for FixedI8<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		i8::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedI16<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		i16::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedI32<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		i32::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedI64<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		i64::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedU8<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		u8::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedU16<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		u16::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedU32<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		u32::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}

impl<Frac> ReprWritable for FixedU64<Frac> {
	fn align(abi: &crate::ReprAbi) -> usize
	where
		Self: Sized,
	{
		u64::align(abi)
	}

	fn align_dyn(&self, abi: &crate::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn crate::ReprWriter) -> crate::Result<()> {
		self.to_bits().write(writer)
	}
}
