// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this

#[cfg(feature = "std")]
use super::{ReprAbi, ReprWritable, ReprWriter, Result, SliceRef};
use core::{slice, str};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct StrRef<'a> {
	data: &'a u8,
	len: usize,
}

impl<'a> From<&'a str> for StrRef<'a> {
	fn from(string: &'a str) -> StrRef<'a> {
		Self {
			data: &string.as_bytes()[0],
			len: string.len(),
		}
	}
}

impl<'a> core::ops::Deref for StrRef<'a> {
	type Target = str;

	fn deref(&self) -> &Self::Target {
		unsafe {
			let slice = slice::from_raw_parts::<'a>(self.data as *const u8, self.len);
			str::from_utf8_unchecked(slice)
		}
	}
}

#[cfg(feature = "std")]
impl<'a> ReprWritable for StrRef<'a> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		usize::max(abi.align_of_ptr, abi.align_of_usize)
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;
		seq.write_reference(&SliceRef::from(self.as_bytes()))?;
		seq.write_element(&self.len)?;
		seq.end()
	}
}
