// repr-writer <https://gitlab.com/demoura/repr-writer>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![cfg_attr(not(feature = "std"), no_std)]

use cfg_if::cfg_if;

mod external;
mod slice_ref;
mod str_ref;

pub use slice_ref::SliceRef;
pub use str_ref::StrRef;

cfg_if! {
	if #[cfg(feature = "std")] {
		mod abi;
		mod artifact_writer;
		mod writer;
		mod writable;
		mod error;

		pub use abi::ReprAbi;
		pub use artifact_writer::{to_global, ArtifactWriter};
		pub use byteorder::{
			ByteOrder,
			BigEndian,
			LittleEndian,
			NativeEndian,
			NetworkEndian
		};
		pub use error::ArtifactWriterError;
		pub use faerie;
		pub use faerie::{Artifact, ArtifactError, Reloc};
		pub use writable::ReprWritable;
		pub use writer::{ReprWriter, ReprSequenceWriter};
		pub use error::Error;

		pub type Result<T> = core::result::Result<T, std::boxed::Box<dyn Error>>;
	}
}
